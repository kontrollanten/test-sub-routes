function register ({
  registerClientRoute,
  registerHook,
  peertubeHelpers
}) {
  registerClientRoute({
    route: '/sub-route',
    parentRoute: '/my-account',
    menuItem: {
      label: 'Sub route',
    },
    onMount: ({ rootEl }) => {
      rootEl.innerText = 'Hi, this is a sub route with menu included.';
    }
  })

  registerClientRoute({
    route: '/sub-route-without-menu-item',
    parentRoute: '/my-account',
    onMount: ({ rootEl }) => {
      rootEl.innerText = 'Hi, this is a sub route without menu item.';
    }
  })
}

export {
  register
}
