function register ({
  registerClientRoute,
  registerHook,
  peertubeHelpers
}) {
  registerClientRoute({
    route: '/regular',
    onMount: ({ rootEl }) => {
      rootEl.innerText = 'Hi, this is a regular client route.';
    }
  })
}

export {
  register
}
